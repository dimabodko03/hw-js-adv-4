const filmsList = document.getElementById('films-list');

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(films => {

        films.forEach(film => {
            const filmContainer = document.createElement('div');
            filmContainer.classList.add('film-container');

            const filmInfo = document.createElement('div');
            filmInfo.classList.add('film-info');
            filmInfo.innerHTML = `
                <h3>Episode ${film.id}: ${film.name}</h3>
                `;

            const charactersList = document.createElement('ul');
            const charactersListLoading = document.createElement('div');
            charactersList.classList.add('characters-list');
            charactersListLoading.classList.add('loading');

            filmContainer.appendChild(filmInfo);
            filmContainer.appendChild(charactersList);
            filmContainer.appendChild(charactersListLoading);
            filmsList.appendChild(filmContainer);

            Promise.all(film.characters.map(characterUrl => {
                return fetch(characterUrl)
                    .then(response => response.json())
                    .then(character => {
                        const characterItem = document.createElement('li');
                        characterItem.textContent = character.name;
                        charactersList.appendChild(characterItem);
                    });
            }))
                .then(() => {
                    charactersListLoading.classList.remove('loading');
                });
        });

    })
    .catch(error => {
        console.error('Error fetching films:', error);
    });
